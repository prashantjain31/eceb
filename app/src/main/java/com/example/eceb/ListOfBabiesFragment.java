package com.example.eceb;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListOfBabiesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListOfBabiesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static int mCaller = 0;

    public ListOfBabiesFragment(int caller) {
        // Required empty public constructor
        mCaller = caller;
    }

    public ListOfBabiesFragment(){
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListOfBabiesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListOfBabiesFragment newInstance(String param1, String param2) {
        ListOfBabiesFragment fragment = new ListOfBabiesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(mCaller == 1) {
            mCaller = 0;
            return inflater.inflate(R.layout.fragment_list_of_babies, container, false);
        } else if(mCaller == 2) {
            mCaller = 0;
            return inflater.inflate(R.layout.discharged, container, false);
        } else if(mCaller == 3) {
            mCaller = 0;
            return inflater.inflate(R.layout.high_risk, container, false);
        } else if(mCaller == 4) {
            mCaller = 0;
            View view =  inflater.inflate(R.layout.register_layout, container, false);
            TextView multipleBabies = (TextView) view.findViewById(R.id.multiple_babies);
            multipleBabies.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setBackground(getResources().getDrawable(R.drawable.rounded_button_small_radius_with_color));
                    SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekbar_for_multiple_babies);
                    seekBar.setVisibility(View.VISIBLE);
                    TextView singleBaby = (TextView) view.findViewById(R.id.single_baby);
                    singleBaby.setBackground(getResources().getDrawable(R.drawable.rounded_button_small_radius));
                }
            });

            TextView singleBaby = (TextView) view.findViewById(R.id.single_baby);
            singleBaby.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setBackground(getResources().getDrawable(R.drawable.rounded_button_small_radius_with_color));
                    SeekBar seekBar = (SeekBar) view.findViewById(R.id.seekbar_for_multiple_babies);
                    seekBar.setVisibility(View.GONE);
                    TextView multipleBabies = (TextView) view.findViewById(R.id.multiple_babies);
                    multipleBabies.setBackground(getResources().getDrawable(R.drawable.rounded_button_small_radius));
                }
            });

            return view;
        }
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_of_babies, container, false);
    }
}