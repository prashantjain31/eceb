package com.example.eceb;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class CategoryAdapter extends FragmentPagerAdapter {

    /** Context of the app */
    private Context mContext;
    private int mCaller = 0;

    public CategoryAdapter(Context context, FragmentManager fm, int caller) {
        super(fm);
        mContext = context;
        mCaller = caller;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new HomeFragment();
        } else if (position == 1) {
            return new ListOfBabiesFragment(mCaller);
        } else if (position == 2) {
            return new NotificationsFragment();
        } else {
            return new ProfileFragment();
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return mContext.getString(R.string.category_home);
        } else if (position == 1) {
            return mContext.getString(R.string.category_list_of_babies);
        } else if (position == 2) {
            return mContext.getString(R.string.category_notifications);
        } else {
            return mContext.getString(R.string.category_profile);
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
