package com.example.eceb;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.android.material.tabs.TabLayout;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView =  inflater.inflate(R.layout.fragment_home, container, false);

        RelativeLayout relativeLayout = (RelativeLayout) rootView.findViewById(R.id.admitted_button);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (HomeFragment.this.getActivity(), MainActivity.class);
                intent.putExtra("Tab", 1);
                intent.putExtra("caller", 1);
                HomeFragment.this.getActivity().startActivity(intent);
            }
        });

        RelativeLayout relativeLayout_2 = (RelativeLayout) rootView.findViewById(R.id.discharged_button);
        relativeLayout_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (HomeFragment.this.getActivity(), MainActivity.class);
                intent.putExtra("Tab", 1);
                intent.putExtra("caller", 2);
                HomeFragment.this.getActivity().startActivity(intent);
            }
        });

        RelativeLayout relativeLayout_3 = (RelativeLayout) rootView.findViewById(R.id.high_risk_button);
        relativeLayout_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (HomeFragment.this.getActivity(), MainActivity.class);
                intent.putExtra("Tab", 1);
                intent.putExtra("caller", 3);
                HomeFragment.this.getActivity().startActivity(intent);
            }
        });

        RelativeLayout relativeLayout_4 = (RelativeLayout) rootView.findViewById(R.id.register_button);
        relativeLayout_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (HomeFragment.this.getActivity(), MainActivity.class);
                intent.putExtra("Tab", 1);
                intent.putExtra("caller", 4);
                HomeFragment.this.getActivity().startActivity(intent);
            }
        });

        return rootView;
    }

}