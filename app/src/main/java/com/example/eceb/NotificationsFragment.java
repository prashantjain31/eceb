package com.example.eceb;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.telecom.TelecomManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link NotificationsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NotificationsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public NotificationsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationsFragment newInstance(String param1, String param2) {
        NotificationsFragment fragment = new NotificationsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        TextView monitoringView = (TextView) view.findViewById(R.id.monitoring);
        monitoringView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout lineLayout2 = (LinearLayout) view.findViewById(R.id.line2);
                lineLayout2.setVisibility(View.VISIBLE);
                LinearLayout lineLayout1 = (LinearLayout) view.findViewById(R.id.line1);
                lineLayout1.setVisibility(View.INVISIBLE);
                LinearLayout riskLayout = (LinearLayout) view.findViewById(R.id.risk_layout);
                riskLayout.setVisibility(View.GONE);
                LinearLayout monitoringLayout = (LinearLayout) view.findViewById(R.id.monitoring_layout);
                monitoringLayout.setVisibility(View.VISIBLE);
            }
        });

        TextView riskView = (TextView) view.findViewById(R.id.risk);
        riskView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinearLayout lineLayout2 = (LinearLayout) view.findViewById(R.id.line2);
                lineLayout2.setVisibility(View.INVISIBLE);
                LinearLayout lineLayout1 = (LinearLayout) view.findViewById(R.id.line1);
                lineLayout1.setVisibility(View.VISIBLE);
                LinearLayout riskLayout = (LinearLayout) view.findViewById(R.id.risk_layout);
                riskLayout.setVisibility(View.VISIBLE);
                LinearLayout monitoringLayout = (LinearLayout) view.findViewById(R.id.monitoring_layout);
                monitoringLayout.setVisibility(View.GONE);
            }
        });

        return view;
    }
}