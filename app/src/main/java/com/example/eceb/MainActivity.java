package com.example.eceb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    CategoryAdapter adapter;
    ViewPager viewPager;
    TabLayout tabLayout;
    private int[] tabIcons = {
            R.drawable.home_tablayout,
            R.drawable.list_tablayout,
            R.drawable.notifications_tablayout,
            R.drawable.profile_tablayout
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        int caller = intent.getIntExtra("caller", 0);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        // Create an adapter that knows which fragment should be shown on each page
        adapter = new CategoryAdapter(this, getSupportFragmentManager(), caller);

        // Set the adapter onto the view pager
        viewPager.setAdapter(adapter);

        // Find the tab layout that shows the tabs
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        // Connect the tab layout with the view pager. This will
        //   1. Update the tab layout when the view pager is swiped
        //   2. Update the view pager when a tab is selected
        //   3. Set the tab layout's tab names with the view pager's adapter's titles
        //      by calling onPageTitle()
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();


        int intent_value = intent.getIntExtra("Tab", 0);
        if(intent_value == 1){
            tabLayout.setScrollPosition(1, 0f, true);
            viewPager.setCurrentItem(1);
        }
    }

    private void setupTabIcons() {
        for(int i = 0; i < 4; i++) {
            tabLayout.getTabAt(i).setIcon(tabIcons[i]);
        }
    }

}